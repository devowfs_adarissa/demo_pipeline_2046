<?php

use PHPUnit\Framework\TestCase;
use Dev\Act10\client;


class clientTest extends TestCase
{

    /**
     * @dataProvider dataForCin
     */
    public function testSetCin($cin)
    {
        $clt = new client();
        $this->expectException(Exception::class);
        $clt->setcin($cin);
    }

    private function dataForCin()
    { //"/^[A-Z]{2,4}\d{2,7}$/"
        return [
            ["12556FD"],
            ["A254564"],
            ["_AT254564"],
            ["AB25456489"],
            ["yyyy27722"],
            ["CD1445"]
        ];
    }
}
