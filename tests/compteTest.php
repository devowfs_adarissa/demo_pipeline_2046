<?php

use Dev\Act10\compte;
use Dev\Act10\client;
use PHPUnit\Framework\TestCase;

class compteTest extends TestCase
{

    /**
     * @dataProvider dataForCrediter
     */
    public function testCrediter($solde1, $solde2, $somme, $nvSolde1, $nvSolde2)
    {
        $compte1 = new compte($solde1, new client());
        $compte2 = new compte($solde2, new client());
        $compte1->créditer($somme, $compte2);
        $this->assertEquals($nvSolde1, $compte1->getSolde());
        $this->assertEquals($nvSolde2, $compte2->getSolde());
    }

    private function dataForCrediter()
    {
        return [
            [0, 0, 0, 0, 0],
            [8000, 10000, 1000, 9000, 9000],
            [100, 1500, 200, 300, 1300]
        ];
    }
}
